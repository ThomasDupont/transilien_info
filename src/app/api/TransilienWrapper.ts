import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {catchError, retry} from "rxjs/operators";
import {throwError} from "rxjs";
import {Injectable} from "@angular/core";
import {DeparturePayload} from "../interface/DeparturePayload";
import {Stop} from "../interface/Stop";
import {TrainItem} from "../interface/TrainItem";

const URL = 'https://transilien-info.herokuapp.com/';

@Injectable({
    providedIn: 'root',
})
export class TransilienWrapper {
    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            console.error('An error occurred:', error.error.message);
        } else {
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        return throwError(
            'Something bad happened; please try again later.');
    }

    constructor(private http: HttpClient) {
    }

    public getSearchInfo() {
        return this.http.get<{
            content: Array<Stop>
        }>(URL + 'v1/train/list/stops')
            .pipe(
                retry(3),
                catchError(this.handleError)
            );
    }

    public getNextDeparture(payload: DeparturePayload) {
        return this.http.post<{
            platformAvailable: boolean,
            disruptionsAvailable: boolean,
            nextTrainsList: Array<TrainItem>
        }>(URL + 'v1/train/search/departure', payload, {
            headers: new HttpHeaders({
                'Content-Type':  'application/json',
            })
        }).pipe(
            retry(3),
            catchError(this.handleError)
        )
    }
}