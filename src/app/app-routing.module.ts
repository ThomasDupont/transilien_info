import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomePageModule'
  },
    {
        path: 'departure',
        loadChildren: './departure/departure.module#DeparturePageModule'
    },
    {
        path: 'arrival',
        loadChildren: './arrival/arrival.module#ArrivalPageModule'
    },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
