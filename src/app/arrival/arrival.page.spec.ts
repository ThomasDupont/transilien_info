import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArrivalPage } from './arrival.page';

describe('ArrivalPage', () => {
  let component: ArrivalPage;
  let fixture: ComponentFixture<ArrivalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArrivalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArrivalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
