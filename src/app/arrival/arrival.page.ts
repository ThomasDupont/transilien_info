import { Component, OnInit } from '@angular/core';
import {SetStation} from "../set-station/SetStation";
import Store from "../store";
import {TransilienWrapper} from "../api/TransilienWrapper";
import {ToastController} from "@ionic/angular";
import {Storage} from "@ionic/storage";

@Component({
  selector: 'app-arrival',
  templateUrl: './arrival.page.html',
  styleUrls: ['./arrival.page.scss'],
})
export class ArrivalPage extends SetStation implements OnInit {

    constructor(
        protected store: Store,
        protected transilientWrapper: TransilienWrapper,
        protected toastController: ToastController,
        protected storage: Storage
    ) {
        super(store, transilientWrapper, toastController, storage);
        this.page = 'arrival';
    }
}
