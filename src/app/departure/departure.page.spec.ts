import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeparturePage } from './departure.page';

describe('DeparturePage', () => {
  let component: DeparturePage;
  let fixture: ComponentFixture<DeparturePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeparturePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeparturePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
