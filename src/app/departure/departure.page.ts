import {Component, OnInit} from '@angular/core';
import { Storage } from '@ionic/storage';
import Store from "../store";
import {TransilienWrapper} from "../api/TransilienWrapper";
import {ToastController} from "@ionic/angular";
import {SetStation} from "../set-station/SetStation";

@Component({
    selector: 'app-departure',
    templateUrl: './departure.page.html',
    styleUrls: ['./departure.page.scss'],
})
export class DeparturePage extends SetStation implements OnInit {

    constructor(
        protected store: Store,
        protected transilientWrapper: TransilienWrapper,
        protected toastController: ToastController,
        protected storage: Storage
    ) {
        super(store, transilientWrapper, toastController, storage);
        this.page = 'departure';
    }
}
