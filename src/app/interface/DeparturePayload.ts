export interface DeparturePayload {
    departure: string,
    uicDeparture: string,
    uicArrival: string,
    pmr: boolean
}