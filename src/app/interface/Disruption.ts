export interface Disruption {
    id: string,
    creationDate: string,
    updateDate: string,
    title: string,
    type: string,
    validityPeriods: Array<{
        startDate: string,
        endDate: string,
        now: boolean
    }>,
    detail: string,
    startingApplicationDate: string,
    hasSubstitutionBus: boolean,
    line: string,
    transport: string
}