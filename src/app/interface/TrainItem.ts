import {Disruption} from "./Disruption";

export interface TrainItem {
    modeTransportEnum: string,
    lineTransportEnum: string,
    codeMission: string,
    cancelled: boolean,
    delayed: boolean,
    departureTime: string,
    destinationMission: string,
    platform: string,
    deservedStations: Array<string>,
    hasTraficDisruption: boolean,
    hasTravauxDisruption: boolean,
    disruptions: Array<Disruption>
}