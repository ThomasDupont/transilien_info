
import Store from "../store";
import {TransilienWrapper} from "../api/TransilienWrapper";
import {ToastController} from "@ionic/angular";
import {Storage} from "@ionic/storage";
import {Stop} from "../interface/Stop";
import {TrainItem} from "../interface/TrainItem";

interface Payload {
    departure: string,
    arrival: string,
    uicDeparture: string,
    uicArrival: string,
    pmr: boolean
}

const TOAST = 0;
const OLD_PAYLOAD = 1;
const FIRST_SUG_DEP = 3;
const FIRST_SUG_ARR = 7;
const OLD_PAYLOAD_DEP = 11;
const SEL_DEP = 13;
const SEL_ARR = 17;

export class SetStation {
    public displaySearchCard: boolean = false;
    public departureSearch: string;
    public arrivalSearch: string;
    public stopList: Array<Stop>;
    public suggestionsDeparture: Array<Stop>;
    public suggestionsArrival: Array<Stop>;
    public departureLabel: string = '';
    public arrivalLabel: string = '';
    public nextDepartures: Array<TrainItem>;
    public payload: Payload;
    public choise: {
        departure?: Stop,
        arrival?: Stop
    } = {
        departure: null,
        arrival: null
    };

    public page: string;

    constructor(
        protected store: Store,
        protected transilientWrapper: TransilienWrapper,
        protected toastController: ToastController,
        protected storage: Storage
    ) {}

    ngOnInit(): void {
        if (!this.store.has('stopList')) {
            this.transilientWrapper.getSearchInfo().subscribe((resp) => {
                this.store.state.stopList = resp.content;
                this.stopList = resp.content;
            });
        }

        this.stopList = this.store.state.stopList;

        this.storage.get(this.page + '_payload').then((payload: Payload) => {
            if (payload) {
                this.departureLabel = payload.departure;
                this.arrivalLabel = payload.arrival;
                this.payload = payload;
                this.getNextDeparture(payload);
            }
        });
    }

    public search(type: number): void {
        let suggestions = this.searchAlgo(type === 0 ? this.departureSearch : this.arrivalSearch);

        if (suggestions.length > 5) {
            suggestions.length = 5;
        }

        if (type === 0) {
            this.departureLabel = '';
            this.suggestionsDeparture = suggestions
        } else {
            this.arrivalLabel = '';
            this.suggestionsArrival = suggestions;
        }
    }

    public selectItem(el: Stop, type: number = 0): void {
        if (type === 0) {
            this.choise.departure = el;
            this.departureLabel = el.label;
            this.suggestionsDeparture = [];
        } else {
            this.choise.arrival = el;
            this.arrivalLabel = el.label;
            this.suggestionsArrival = [];
        }
    }

    public save(): boolean {
        let payload: Payload = {
            departure : '',
            uicDeparture: '',
            arrival: '',
            uicArrival: '',
            pmr: false
        };

        switch (this.searchState()) {
            case OLD_PAYLOAD:
                payload = this.payload;
            case OLD_PAYLOAD_DEP * FIRST_SUG_ARR:
                payload.arrival = this.suggestionsArrival[0].label;
                payload.uicArrival = this.suggestionsArrival[0].uic;
                break;
            case SEL_DEP:
                payload.departure = this.choise.departure.label;
                payload.departure = this.choise.departure.uic;
                break;
            case SEL_DEP * SEL_ARR:
                payload.departure = this.choise.departure.label;
                payload.departure = this.choise.departure.uic;
                payload.departure = this.choise.arrival.label;
                payload.departure = this.choise.arrival.uic;
                break;
            case SEL_DEP * FIRST_SUG_ARR:
                payload.departure = this.choise.departure.label;
                payload.departure = this.choise.departure.uic;
                payload.arrival = this.suggestionsArrival[0].label;
                payload.uicArrival = this.suggestionsArrival[0].uic;
                break;
            case FIRST_SUG_DEP * SEL_ARR:
                payload.departure = this.suggestionsDeparture[0].label;
                payload.uicDeparture = this.suggestionsDeparture[0].uic;
                payload.departure = this.choise.arrival.label;
                payload.departure = this.choise.arrival.uic;
                break;
            case FIRST_SUG_DEP * FIRST_SUG_ARR:
                payload.departure = this.suggestionsDeparture[0].label;
                payload.uicDeparture = this.suggestionsDeparture[0].uic;
                payload.departure = this.suggestionsArrival[0].label;
                payload.departure = this.suggestionsArrival[0].uic;
                break;
            case FIRST_SUG_DEP:
                payload.departure = this.suggestionsDeparture[0].label;
                payload.uicDeparture = this.suggestionsDeparture[0].uic;
                break;
            default:
                this.toast('Merci de choisir une gare de départ');
                return false;

        }

        this.storage.set(this.page + '_payload', payload).then(() => {
            this.getNextDeparture(payload, true);
        });

        return true;
    }

    public clearSearch() {

    }

    private searchState(): number {
        if (
            (!this.departureSearch && this.arrivalSearch && !this.arrivalLabel) ||
            (!this.departureSearch && this.arrivalSearch && this.arrivalLabel)
        ) {
            if (this.payload) {
                return this.payload.departure ? (OLD_PAYLOAD_DEP * FIRST_SUG_ARR) : TOAST;
            }

            return TOAST;
        }

        if (this.departureSearch && this.arrivalSearch && this.departureLabel && this.arrivalLabel) {
            return SEL_DEP * SEL_ARR;
        }

        if (this.departureSearch && this.arrivalSearch && this.departureLabel && !this.arrivalLabel) {
            return SEL_DEP * FIRST_SUG_ARR;
        }

        if (this.departureSearch && this.arrivalSearch && !this.departureLabel && this.arrivalLabel) {
            return FIRST_SUG_DEP * SEL_ARR
        }

        if (this.departureSearch && this.arrivalSearch && !this.departureLabel && !this.arrivalLabel) {
            return FIRST_SUG_DEP * FIRST_SUG_ARR;
        }

        if (this.departureSearch && !this.arrivalSearch && !this.departureLabel) {
            return FIRST_SUG_DEP;
        }

        if (this.departureSearch && this.departureLabel) {
            return SEL_DEP;
        }

        return (this.payload === undefined || this.payload === null) ? TOAST : OLD_PAYLOAD;
    }

    private getNextDeparture(payload: Payload, manual: boolean = false): void {
        if (this.store.has('next_' + this.page) && !manual) {
            this.nextDepartures = this.store.state['next_' + this.page];
        } else {
            this.toast('Récupération des départs');
            this.transilientWrapper.getNextDeparture(payload).subscribe(resp => {
                this.nextDepartures = resp.nextTrainsList;
                this.store.set('next_' + this.page, resp.nextTrainsList, 20);
            }, error => this.toast('La récupération des départs à echouer'));
        }
    }

    private searchAlgo(reg: string): Array<Stop> {
        return this.stopList.filter((stop: Stop) => {
            return stop.label.match(new RegExp(reg, 'i'));
        });
    }

    private toast(message: string, duration: number = 2000) {
        this.toastController.create({ message, duration }).then((t) => t.present());
    }
}
