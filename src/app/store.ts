import {TransilienWrapper} from "./api/TransilienWrapper";
import {Injectable} from "@angular/core";
import {Stop} from "./interface/Stop";
import {DeparturePayload} from "./interface/DeparturePayload";

const state: {
    [index: string]: any,
    stopList?: Array<Stop>,
    departurePayload?: DeparturePayload,
    arrivalPayload?: DeparturePayload
} = {};

@Injectable({
    providedIn: 'root',
})
export default class Store {
    public state: {
        [index: string]: any,
        stopList?: Array<Stop>,
        departurePayload?: DeparturePayload,
        arrivalPayload?: DeparturePayload
    } = {};

    public timeoutManager: {
        [index: string]: any,
    } = {};

    constructor(private transilienWrapper: TransilienWrapper) {
        this.state = state;
        this.transilienWrapper.getSearchInfo().subscribe((resp) => {
            this.state.stopList = resp.content;
        });
    }

    has(prop: string): boolean {
        return this.state[prop] !== undefined && this.state[prop] !== null;
    }

    set (key: string, value: any, expire: number = 36000): void {
        if (expire <= 0) {
            throw new Error('Cache timeout must be a positive number');
        }
        this.state[key] = value;
        this.timeoutManager[key] = setTimeout(() => {
            this.del(key);
        }, expire * 1000);
    };

    del(key: string): void {
        this.state[key] = null;
        clearTimeout(this.timeoutManager[key]);
        delete this.timeoutManager[key];
    };
}